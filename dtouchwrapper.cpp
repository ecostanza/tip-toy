/***************************************************************************
    dtouchwrapper.cpp  -  wrapper class to expose dtouch to QML
                             -------------------
    copyright            : (C) 2019 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of libdtouch.                                         *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

#include "dtouchwrapper.h"
#include <QDebug>
#include <QCamera>

QVideoFilterRunnable * DtouchFilter::createFilterRunnable()
{
    return new DtouchFilterRunnable(this);
}

DtouchFilterRunnable::DtouchFilterRunnable(DtouchFilter *filter) :
    _filter(filter),
    _w(0),
    _h(0),
    _grayData(nullptr),
    //_grayDataDrawable(nullptr),
    _readOnly(false),
    _fr(nullptr),
    _gi(nullptr),
    _fiducialTracking(nullptr),
    _convolver(nullptr),
    _projectiveTransform(nullptr)
{
}

DtouchFilterRunnable::~DtouchFilterRunnable()
{
    stop();
}

bool DtouchFilterRunnable::start(const QVideoSurfaceFormat &surfaceFormat)
{
    _rgbFormats << QVideoFrame::Format_RGB24;
    _rgbFormats << QVideoFrame::Format_BGR24;
    _rgbFormats << QVideoFrame::Format_RGB32;
    _rgbFormats << QVideoFrame::Format_ARGB32;
    _rgbFormats << QVideoFrame::Format_BGRA32;
    _rgbFormats << QVideoFrame::Format_BGR32;
    _rgbFormats << QVideoFrame::Format_ARGB32_Premultiplied;
    _rgbFormats << QVideoFrame::Format_BGRA32_Premultiplied;
//    for (int i=0; i < _rgbFormats.size(); i++) {
//        qDebug() << "_rgbFormats[" << i << "]: " << _rgbFormats[i];
//    }

    _h = surfaceFormat.frameHeight();
    _w = surfaceFormat.frameWidth();
    int size = _w * _h;
    _grayData = new unsigned char[size];
    _rgbData = new unsigned char[size*3];

    qDebug() << "start: " << _w << ", " << _h;
    qDebug() << "pixelFormat: " << surfaceFormat.pixelFormat();

    _fr = FiducialRecognition::newL(_w, _h, 40, 32);
    _fr->addSequenceL("{3,3,3}",false);
    _fr->addSequenceL("{0,1,2,3,4}",true);
    if ( _rgbFormats.contains(surfaceFormat.pixelFormat()) ) {
        qDebug() << "_rgbFormats.contains(surfaceFormat.pixelFormat())";
        _gi = new GraphicImageRGB(_w,_h);
    } else {
        _gi = new GraphicImageGrayscale(_w,_h);
    }

    _projectiveTransform = new ProjectiveTransform(_w, _h);
    //TODO: move filter size to member var/UI?
    _convolver = new Convolver(3, _w, _h);
    //TODO: move these parameters to member var, command line?
    int dist = _w / 12;
    int tracking = 5;
    _filter->_trackingDistance = dist;
    _filter->_trackingThreshold = tracking;
    _fiducialTracking = FiducialTracking::newL(dist,tracking);

    return true;
}

void DtouchFilterRunnable::stop()
{
    delete _fr;
    _fr = nullptr;
    delete _gi;
    _gi = nullptr;

    delete _fiducialTracking;
    _fiducialTracking = nullptr;
    delete _projectiveTransform;
    _projectiveTransform = nullptr;
    delete _convolver;
    _convolver = nullptr;

    delete [] _grayData;
    delete [] _rgbData;
}

QVideoFrame DtouchFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags)
{
    (void)flags;

    QAbstractVideoBuffer::MapMode mapMode = QAbstractVideoBuffer::ReadWrite;
    if (_readOnly) {
        mapMode = QAbstractVideoBuffer::ReadOnly;
    }

    if (!input->map(mapMode)) {
        qDebug() << "cannot map video buffer in " << mapMode << " mode";

        if (_readOnly) {
            return *input;
        } else {
            _readOnly = true;
            if (!input->map(QAbstractVideoBuffer::ReadOnly)) {
                qDebug() << "cannot map video buffer in ReadWrite mode";
                return *input;
            }
        }

    }


    if (_fr == nullptr) {
        start(surfaceFormat);
    } else if (
               _h != surfaceFormat.frameHeight() ||
               _w != surfaceFormat.frameWidth()
               ) {
        stop();
        start(surfaceFormat);
    }
    unsigned char * grayDataPtr = _grayData;
    unsigned char * rgbDataPtr = _rgbData;

    unsigned char * srcPtr = input->bits();

    int size = _w * _h;

    if ( _rgbFormats.contains(surfaceFormat.pixelFormat()) ) {
        unsigned char r, g, b, value;
        if (
                surfaceFormat.pixelFormat() == QVideoFrame::Format_RGB24 ||
                surfaceFormat.pixelFormat() == QVideoFrame::Format_BGR24
                ) {
            for (int i=0; i<size; i++) {
                r = *srcPtr++;
                g = *srcPtr++;
                b = *srcPtr++;
                *rgbDataPtr++ = r;
                *rgbDataPtr++ = g;
                *rgbDataPtr++ = b;
                value = (r + g + b) / 3;
                *grayDataPtr++ = value;
            }
        } else if (
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_BGRA32_Premultiplied ||
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_BGRA32 ||
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_BGR32 ||
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_RGB32
                   ) {
            for (int i=0; i<size; i++) {
                r = *srcPtr++;
                g = *srcPtr++;
                b = *srcPtr++;
                srcPtr++; // a
                *rgbDataPtr++ = r;
                *rgbDataPtr++ = g;
                *rgbDataPtr++ = b;
                value = (r + g + b) / 3;
                *grayDataPtr++ = value;
            }
        } else if (
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_ARGB32 ||
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_ABGR32 ||
                   surfaceFormat.pixelFormat() == QVideoFrame::Format_ARGB32_Premultiplied
                   ) {
            // It seems that on OSX even though the format is set to ARGB32
            // it is actually RGBA32 :-(
            for (int i=0; i<size; i++) {
                r = *srcPtr++;
                g = *srcPtr++;
                b = *srcPtr++;
                srcPtr++; // a
                *rgbDataPtr++ = r;
                *rgbDataPtr++ = g;
                *rgbDataPtr++ = b;
                value = (r + g + b) / 3;
                *grayDataPtr++ = value;
            }
        } else {
            qDebug() << "unsupported format" << surfaceFormat.pixelFormat();
            input->unmap();
            return *input;
        }
        _gi->setImageBuffer(&_rgbData);
    } else {
        if (surfaceFormat.pixelFormat() == QVideoFrame::Format_YUYV) {
            unsigned char value;
            // for simplicity and speed simply get rgb to be grayscale
            for (int i=0; i<size; i+= 2) {
                value = *srcPtr++; // Y
                srcPtr++; // U
                *grayDataPtr++ = value;
                *rgbDataPtr++ = value;
                *rgbDataPtr++ = value;
                *rgbDataPtr++ = value;

                value = *srcPtr++; // Y
                srcPtr++; // V
                *grayDataPtr++ = value;
                *rgbDataPtr++ = value;
                *rgbDataPtr++ = value;
                *rgbDataPtr++ = value;
            }

            _gi->setImageBuffer(&_rgbData);
        } else {
            // unknown pixel format!
            qDebug() << "unsupported format" << surfaceFormat.pixelFormat();
            input->unmap();
            return *input;
        }
    }

    if (_filter->_blur) {
        _convolver->convolveMono(_grayData, _w, _h, 0, 0, _w, _h);
    }

    FiducialDataList * results = nullptr;
    if(!_readOnly) {
        results = _fr->process(_grayData, _gi, _filter->_displayThreshold, true);
    } else {
        results = _fr->process(_grayData, NULL, false, true);
    }

    if( _filter->_tracking ){
        _fiducialTracking->distanceThreshold() = _filter->_trackingDistance;
        _fiducialTracking->trackingThreshold() = _filter->_trackingThreshold;
        //qDebug() << "tracking:" << _filter->_trackingDistance << "|" << _filter->_trackingThreshold;
        if(results==nullptr){
            results = FiducialDataList::newL();
        }
        _fiducialTracking->process(results);
    }

    if (results != nullptr) {
        // calibration
        int calibCount = 0;
        if( _filter->_calibration ){
            DTPoint calibrationPoint[4];
            for( results->reset(); !(results->nullTest()) && (calibCount<4); results->fwd() ){
                FiducialData curr = results->getData();
                if( curr.getType() == CALIBRATION_TYPE ){
                    calibrationPoint[calibCount] = curr.getCentre();
                    calibCount++;
                }
            }
            //qDebug() << "calibrationCount:" << calibCount;

            if( calibCount == 4 ){
                _projectiveTransform->calculateCalibMatrix( calibrationPoint, _w, _h );
            }
        }

        DtouchResult * dt_result = new DtouchResult();
        for(results->reset(); !results->isNull(); results->fwd()) {
            FiducialData tmp = results->getData();
            QVariantMap map;
            DTPoint centre;
            if (_filter->_flipImage) {
                if (_filter->_rotateImage) {
                    centre = _projectiveTransform->projectRotateCW(tmp.Centre());
                } else {
                    centre = _projectiveTransform->projectFlip(tmp.Centre());
                }
            } else {
                if (_filter->_rotateImage) {
                    centre = _projectiveTransform->projectRotateCCW(tmp.Centre());
                } else {
                    centre = _projectiveTransform->project(tmp.Centre());
                }
            }
            //DTPoint centre = _projectiveTransform->projectFlip(tmp.Centre());
            //DTPoint centre = _projectiveTransform->projectRotateCW(tmp.Centre());
            map.insert("x", QVariant(static_cast<float>(static_cast<float>(centre.x) / this->_w)));
            map.insert("y", QVariant(static_cast<float>(static_cast<float>(centre.y)/this->_h)));
            map.insert("angle", QVariant(tmp.getAngle()));
            map.insert("width", QVariant(static_cast<float>(static_cast<float>(tmp.Width())/this->_w)));
            map.insert("height", QVariant(static_cast<float>(static_cast<float>(tmp.Height())/this->_h)));
            int type = 0;
            int factor = 1;
            for (int i=tmp.getTypeArray()[0]; i>1; i--) {
                type += tmp.getTypeArray()[i] * factor;
                factor *= 10;
            }
            map.insert("type", QVariant(type));
            map.insert("origType", QVariant(tmp.Type()));
            qDebug() << "marker type:" << type;

            dt_result->m_markers << map;
        }
        emit _filter->finished(dt_result);

        delete results;
    }

    if(!_readOnly) {
        qDebug() << "pixel format" << surfaceFormat.pixelFormat();
        if ( _rgbFormats.contains(surfaceFormat.pixelFormat()) ) {
            unsigned char * dstPtr = input->bits();
            unsigned char * srcPtr = _rgbData;
            if (
                    surfaceFormat.pixelFormat() == QVideoFrame::Format_RGB24 ||
                    surfaceFormat.pixelFormat() == QVideoFrame::Format_BGR24
                    ) {
                qDebug() << "RGB/BGR 24";
                for (int i=0; i<size; i++) {
                    *dstPtr++ = *srcPtr++; // r
                    *dstPtr++ = *srcPtr++; // g
                    *dstPtr++ = *srcPtr++; // b
                }
            } else if (
                       surfaceFormat.pixelFormat() == QVideoFrame::Format_BGRA32_Premultiplied ||
                       surfaceFormat.pixelFormat() == QVideoFrame::Format_BGRA32 ||
                       surfaceFormat.pixelFormat() == QVideoFrame::Format_BGR32
                       ) {
                qDebug() << "RGB/BGR A 32";
                for (int i=0; i<size; i++) {
                    *dstPtr++ = *srcPtr++; // r
                    *dstPtr++ = *srcPtr++; // g
                    *dstPtr++ = *srcPtr++; // b
                    *dstPtr++ = 255; // a
                }
            } else if (
                       surfaceFormat.pixelFormat() == QVideoFrame::Format_ABGR32 ||
                       surfaceFormat.pixelFormat() == QVideoFrame::Format_ARGB32 ||
                       surfaceFormat.pixelFormat() == QVideoFrame::Format_ARGB32_Premultiplied
                       ) {
                qDebug() << "A RGB/BGR 32";
                for (int i=0; i<size; i++) {
                    *dstPtr++ = *srcPtr++; // r
                    *dstPtr++ = *srcPtr++; // g
                    *dstPtr++ = *srcPtr++; // b
                    dstPtr++; // a
                    //*dstPtr++ = 255; // a
                }
            } else {
                qDebug() << "other format" << surfaceFormat.pixelFormat();
                for (int i=0; i<size; i++) {
                    *dstPtr++ = 255; // a
                    *dstPtr++ = *srcPtr++; // r
                    *dstPtr++ = *srcPtr++; // g
                    *dstPtr++ = *srcPtr++; // b
                }
            }
        }
    }

    input->unmap();

    return *input;
}
