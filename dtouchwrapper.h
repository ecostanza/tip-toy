/***************************************************************************
    dtouchwrapper.h  -  wrapper class to expose dtouch to QML
                             -------------------
    copyright            : (C) 2019 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of libdtouch.                                         *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

#ifndef DTOUCHWRAPPER_H
#define DTOUCHWRAPPER_H

#include <QObject>
#include <QVideoFrame>
#include <QAbstractVideoFilter>
#include <QCamera>
#include <QCameraViewfinderSettings>

#include "libdtouch/fiducialrecognition.h"
#include "libdtouch/fiducialTracking.h"
#include "utils/convolver.h"
#include "utils/projectivetransform.h"

#define CALIBRATION_TYPE 0



class DtouchFilter: public QAbstractVideoFilter {
    Q_OBJECT
public:
    DtouchFilter() : QAbstractVideoFilter(),
        _blur(true),
        _tracking(true),
        _displayThreshold(false),
        _flipImage(false),
        _calibration(true),
        _trackingThreshold(2),
        _trackingDistance(2)
    { }
    QVideoFilterRunnable *createFilterRunnable();
signals:
    void finished(QObject *result);

protected:
    bool _blur;
    bool _tracking;
    bool _displayThreshold;
    bool _flipImage;
    bool _rotateImage;
    bool _calibration;
    int _trackingThreshold;
    int _trackingDistance;
    Q_PROPERTY(bool blur MEMBER _blur NOTIFY blurChanged)
    Q_PROPERTY(bool tracking MEMBER _tracking NOTIFY trackingChanged)
    Q_PROPERTY(bool displayThresholded MEMBER _displayThreshold NOTIFY displayThresholdChanged)
    Q_PROPERTY(bool flipImage MEMBER _flipImage NOTIFY flipImageChanged)
    Q_PROPERTY(bool rotateImage MEMBER _rotateImage NOTIFY rotateImageChanged)
    Q_PROPERTY(bool calibration MEMBER _calibration NOTIFY calibrationChanged)
    Q_PROPERTY(int trackingThreshold MEMBER _trackingThreshold NOTIFY trackingThresholdChanged)
    Q_PROPERTY(int trackingDistance MEMBER _trackingDistance NOTIFY trackingDistanceChanged)


public:
    friend class DtouchFilterRunnable;

public slots:
    bool requestPixelFormat(QObject * obj) {
        // TODO: make a list of supported formats, in order of preference
        // go through the list and stop as soon as one is found

        QList<QVideoFrame::PixelFormat> desired;
        desired << QVideoFrame::Format_NV12;
        desired << QVideoFrame::Format_NV21;
        desired << QVideoFrame::Format_YUV420P;
        desired << QVideoFrame::Format_YV12;
        desired << QVideoFrame::Format_IMC1;
        desired << QVideoFrame::Format_IMC2;
        desired << QVideoFrame::Format_IMC3;
        desired << QVideoFrame::Format_IMC4;
        desired << QVideoFrame::Format_Y8;
        desired << QVideoFrame::Format_RGB24;
        desired << QVideoFrame::Format_BGR24;
        desired << QVideoFrame::Format_RGB32;
        desired << QVideoFrame::Format_BGR32;
        desired << QVideoFrame::Format_ARGB32;
        desired << QVideoFrame::Format_BGRA32;
        desired << QVideoFrame::Format_ARGB32_Premultiplied;
        desired << QVideoFrame::Format_BGRA32_Premultiplied;

        QCamera * camera = qvariant_cast<QCamera*>(obj->property("mediaObject"));
        QList<QVideoFrame::PixelFormat> formats = camera->supportedViewfinderPixelFormats();
        for (int i=0; i < formats.size(); i++) {
            qDebug() << "supported formats[" << i << "]: " << formats[i];
        }

        QCameraViewfinderSettings viewfinderSettings;

        for (int i=0; i<desired.size(); i++) {
            if (formats.contains(desired[i])) {
                viewfinderSettings.setPixelFormat(desired[i]);
                camera->setViewfinderSettings(viewfinderSettings);
                qDebug() << "requested PixelFormat: " << desired[i];
                return true;
            }
        }

        //viewfinderSettings.setPixelFormat(QVideoFrame::Format_NV12);
        viewfinderSettings.setPixelFormat(QVideoFrame::Format_RGB32);

        qDebug() << "requestPixelFormat returning";
        return false;
    }

signals:
    void blurChanged(bool);
    void trackingChanged(bool);
    void displayThresholdChanged(bool);
    void flipImageChanged(bool);
    void rotateImageChanged(bool);
    void calibrationChanged(bool);
    void trackingThresholdChanged(int);
    void trackingDistanceChanged(int);
};


class DtouchFilterRunnable : public QVideoFilterRunnable {


protected:

    DtouchFilter * _filter;
    int _w, _h;
    unsigned char * _grayData;
    unsigned char * _rgbData;
    bool _readOnly;
    //unsigned char * _grayDataDrawable;

    FiducialRecognition * _fr;
    GraphicImage * _gi;

    FiducialTracking * _fiducialTracking;
    Convolver * _convolver;
    ProjectiveTransform * _projectiveTransform;

    QList<QVideoFrame::PixelFormat> _rgbFormats;

    bool start(const QVideoSurfaceFormat &frame);
    void stop();
public:
    DtouchFilterRunnable(DtouchFilter *filter);
    ~DtouchFilterRunnable();
    QVideoFrame run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags);
};

class DtouchResult : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList markers READ markers CONSTANT)

public:
    DtouchResult () : m_markers() { }
    QVariantList markers() const { return m_markers; }

protected:
    QVariantList m_markers;
    friend class DtouchFilterRunnable;
};


#endif // DTOUCHWRAPPER_H
