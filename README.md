# README #

TIP-Toy is a tactile and inclusive open-source toolkit, to allow children with different visual abilities to learn about computational topics through music by combining a series of physical blocks.

It was designed and developed by 

* Enrico Costanza, 
* Giulia Barbareschi and 
* Cathy Holloway 

at University College London.

TIP-Toy is open source, released under GPLv3, and it is built on [Qt](https://qt.io). To compile TIP-Toy, please download and install the QtCreator ide. 

At this point pre-compiled binaries are not available.



