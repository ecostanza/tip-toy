/***************************************************************************
                   convolver.cpp  -  fast integer gaussian blur
									 converted from 
					http://incubator.quasimondo.com/processing/fastblur.pde
									 by Mario Klingemann
                             -------------------
    begin                : Mon Jul 30 2007
    copyright            : (C) 2007 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/


/***************************************************************************
 *                                                                         *
 *	This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 59 Temple Place, Suite 330                           *
 *  Boston, MA  02111-1307  USA                                            *
 *                                                                         *
 ***************************************************************************/


#include "convolver.h"

#include <string.h>

Convolver::Convolver(int sz, const int& width, const int& height){
	_imgBuff = NULL;
	_kernel = NULL;
	_mult = NULL;
	_radius = 0;
	this->init(sz, width, height);
}

Convolver::~Convolver(){
	if( _kernel!=NULL ){
		delete [] _kernel;
		_kernel=NULL;
	}
	if( _mult!=NULL ){
		for(int i=0;i<_kernelSize;i++){
			delete [] _mult[i];
		}
		delete [] _mult;
		_mult=NULL;
	}
}

void Convolver::init(int sz, const int& width, const int& height){
	int i=0;
    int j=0;
	sz=min(max(1,sz),248);
	if( _radius!=sz ){
		_kernelSize=1+sz*2;
		_radius=sz;
 		_kernel = new int[_kernelSize];
		memset(_kernel,0,_kernelSize*sizeof(int));
		_mult = new int * [_kernelSize];
		for(i=0;i<_kernelSize;i++){
			_mult[i] = new int[256];
			memset(_mult[i],0,256*sizeof(int));
		}

		int	sum=0;
		for	(i=1;i<sz;i++){
			int szi=sz-i;
			_kernel[sz+i]=_kernel[szi]=szi*szi;
			sum+=_kernel[szi]+_kernel[szi];
			for (j=0;j<256;j++){
				_mult[sz+i][j]=_mult[szi][j]=_kernel[szi]*j;
			}
		}
		_kernel[sz]=sz*sz;
		sum+=_kernel[sz];
		for	(j=0;j<256;j++){
			_mult[sz][j]=_kernel[sz]*j;
		}
	}
	if (width > 0 && height > 0) {
		_imgBuff = new int[width*height];
	}
}

// this assumes img is an RGB buffer
void Convolver::convolve(unsigned char * img, int in_width, int in_height, int x, int y, int w, int h){

	int sum=0;
        int cr=0;
        int cg=0;
        int cb=0;
	int read=0;
        int i=0;
        int ri=0;
        int xl=0;
        int yl=0;
        int yi=0;
        int ym=0;
        int riw;

	int iw=in_width;
	int wh=iw*in_height;

	int * r = new int[wh];
	int * g = new int[wh];
	int * b = new int[wh];

	unsigned char * ip = img;
	int * rp = r;
	int * gp = g;
	int * bp = b;

	for(i=0;i<wh;i++){
		*rp++ = *ip++;
		*gp++ = *ip++;
		*bp++ = *ip++;
	}

	int	* r2 = new int[wh];
	int	* g2 = new int[wh];
	int	* b2 = new int[wh];

	x=max(0,x);
	y=max(0,y);
	w=x+w-max(0,(x+w)-iw);
	h=y+h-max(0,(y+h)-in_height);
	yi=y*iw;

	for	(yl=y;yl<h;yl++){
		for (xl=x;xl<w;xl++){
			cb=cg=cr=sum=0;
			ri=xl-_radius;
			for	(i=0;i<_kernelSize;i++){
				read=ri+i;
				if (read>=x && read<w){
					read+=yi;
					cr+=_mult[i][r[read]];
					cg+=_mult[i][g[read]];
					cb+=_mult[i][b[read]];
					sum+=_kernel[i];
				}
			}
			ri=yi+xl;
			r2[ri]=cr/sum;
			g2[ri]=cg/sum;
			b2[ri]=cb/sum;
		}
		yi+=iw;
	}
	yi=y*iw;

	int x2=0;
	int yi2 = y*iw*3;
	for	(yl=y;yl<h;yl++){
		ym=yl-_radius;
		riw=ym*iw;
		for (xl=x;xl<w;xl++){
			cb=cg=cr=sum=0;
			ri=ym;
			read=xl+riw;
			for	(i=0;i<_kernelSize;i++){
				if (ri<h && ri>=y){
					cr+=_mult[i][r2[read]];
					cg+=_mult[i][g2[read]];
					cb+=_mult[i][b2[read]];
					sum+=_kernel[i];
				}
				ri++;
				read+=iw;
			}
			x2 = xl + xl + xl;
			img[x2+yi2+0] = (unsigned char)((int)cr/(int)sum);
			img[x2+yi2+1] = (unsigned char)((int)cg/(int)sum);
			img[x2+yi2+2] = (unsigned char)((int)cb/(int)sum);
		}
		yi+=iw;
		yi2+=iw+iw+iw;
	}

	//delete all
	delete [] r;
	delete [] g;
	delete [] b;

	delete [] r2;
	delete [] g2;
	delete [] b2;
}


// this assumes img is an RGB buffer
void Convolver::convolveMono(unsigned char * img, int in_width, int in_height, int x, int y, int w, int h){
	
	int sum=0;
	int cg=0;
	int read=0;
	int i=0;
	int ri=0;
	int xl=0;
	int yl=0;
	int yi=0;
	int ym=0;
	int riw;
	
	int iw=in_width;
	int wh=iw*in_height;
	
	bool allocated = false;
	if (_imgBuff == NULL){
		_imgBuff = new int[wh];
		allocated = true;
	}
	
	x=max(0,x);
	y=max(0,y);
	w=x+w-max(0,(x+w)-iw);
	h=y+h-max(0,(y+h)-in_height);
	yi=y*iw;
	
	//printf("blur init done; (%d,%d) -> (%d,%d)", x, y, w, h);
	
	for	(yl=y;yl<h;yl++){
		for (xl=x;xl<w;xl++){
			cg = sum = 0;
			ri=xl-_radius;
			for	(i=0;i<_kernelSize;i++){
				read=ri+i;
				if (read>=x && read<w){
					read+=yi;
					cg+=_mult[i][img[read]];
					sum+=_kernel[i];
				}
			}
			ri=yi+xl;
			_imgBuff[ri]=cg/sum;
		}
		yi+=iw;
	}
	yi=y*iw;
	
	//printf("out of first for");
	
	int x2=0;
	int yi2 = y*iw;
	for	(yl=y;yl<h;yl++){
		ym=yl-_radius;
		riw=ym*iw;
		for (xl=x;xl<w;xl++){
			cg = sum = 0;
			ri=ym;
			read=xl+riw;
			for	(i=0;i<_kernelSize;i++){
				if (ri<h && ri>=y){
					cg+=_mult[i][_imgBuff[read]];
					sum+=_kernel[i];
				}
				ri++;
				read+=iw;
			}
			x2 = xl;
			img[x2+yi2] = (unsigned char)((int)cg/(int)sum);
		}
		yi+=iw;
		yi2+=iw;
	}
	
	if (allocated) {
		//delete all
		delete [] _imgBuff;
	}

}


void filter( unsigned char * img, const int& w, const int& h, const int& radius ){
	Convolver c(radius);
	c.convolve(img,w,h,0,0,w,h);
	return;
}

void filterMono( unsigned char * img, const int& w, const int& h, const int& radius ){
	Convolver c(radius);
	c.convolveMono(img,w,h,0,0,w,h);
	return;
}
