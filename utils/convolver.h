/***************************************************************************
                   convolver.h  -   fast integer gaussian blur
									converted from 
					http://incubator.quasimondo.com/processing/fastblur.pde
									by Mario Klingemann
                             -------------------
    begin                : Mon Jul 30 2007
    copyright            : (C) 2007 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/


/***************************************************************************
 *                                                                         *
 *	This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 59 Temple Place, Suite 330                           *
 *  Boston, MA  02111-1307  USA                                            *
 *                                                                         *
 ***************************************************************************/



#ifndef _EC_CONVOLVER
#define _EC_CONVOLVER

#include <algorithm>
using namespace std;

class Convolver{
protected:
	int _radius;
	int _kernelSize;
	int * _kernel;
	int ** _mult;
	
	int * _imgBuff;

public:
	Convolver(int sz, const int& width=0, const int& height=0);

	~Convolver();

	void init(int sz, const int& width, const int& height);

	// this assumes img is an RGB buffer
	void convolve(unsigned char * img, int in_width, int in_height, 
		int x, int y, int w, int h);
	
	// this assumes img is a greyscale buffer
	void convolveMono(unsigned char * img, int in_width, int in_height, 
				  int x, int y, int w, int h);
	
};

void filter( unsigned char * img, const int& w, const int& h, const int& radius );
void filterMono( unsigned char * img, const int& w, const int& h, const int& radius );

#endif // _EC_CONVOLVER
