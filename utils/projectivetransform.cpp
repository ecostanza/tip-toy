/***************************************************************************
     projectivetransform.cpp  - 
                             -------------------
    copyright            : (C) 2007 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of DTServer.                                          *
 *                                                                         *
 * DTServer is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * DTServer is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with DTServer. If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/

#include "projectivetransform.h"

void camera2GL( DTPoint in, int w, int h, float& outx, float& outy  ){
	outx = (float) (2 * in.x) / (float) w - 1.0f;
	outy = 1.0f - (float) (2 * in.y) / (float) h;
	return;
}
	
void camera2GL( int inx, int iny, int w, int h, float& outx, float& outy  ){
	outx = (float) (2 * inx) / (float) w - 1.0f;
	outy = 1.0f - (float) (2 * iny) / (float) h;
	return;
}



ProjectiveTransform::ProjectiveTransform( int in_outWidth, int in_outHeight ) : 
	outWidth(in_outWidth), outHeight(in_outHeight)
{
	for(int i=0;i<2;i++){
		for(int j=0;j<3;j++){
			a[i][j] = 0.0;
		}
		c[i] = 0.0;
	}
	a[0][0] = a[1][1] = 1.0;
}

DTPoint ProjectiveTransform::project( DTPoint p ){
	DTPoint result;
	
	double den = (p.x*c[0]*a[1][1]+a[0][0]*p.y*c[1]-a[0][0]*a[1][1]
	-p.y*c[0]*a[0][1]-a[1][0]*p.x*c[1]+a[1][0]*a[0][1]);
	
    result.x = static_cast<int>((p.x*c[1]*a[1][2]-a[1][1]*p.x-a[0][2]*p.y*c[1]+
	a[0][2]*a[1][1]-a[0][1]*a[1][2]+p.y*a[0][1])
	/den);
	
    result.y = static_cast<int>(-(p.x*c[0]*a[1][2]+a[0][0]*p.y-a[0][0]*a[1][2]
	-p.y*c[0]*a[0][2]-a[1][0]*p.x+a[1][0]*a[0][2])
	/den);
	
	return result;
}

DTPoint ProjectiveTransform::projectFlip( DTPoint p ){
    DTPoint result = this->project(p);

    //result.x = outWidth - result.x;
	result.y = outHeight - result.y;

	return result;
}

DTPoint ProjectiveTransform::projectRotateCW( DTPoint p ){
    DTPoint result = this->project(p);

    int tmp = result.x;
    result.x = result.y;
    result.y = tmp;
//    result.x = outWidth - result.x;
//    result.y = outHeight - result.y;

    return result;
}

DTPoint ProjectiveTransform::projectRotateCCW( DTPoint p ){
    DTPoint result = this->project(p);

    //result.x = outWidth - result.x;
    result.y = outHeight - result.y;

    int tmp = result.x;
    result.x = result.y;
    result.y = tmp;

    return result;
}

DTPoint ProjectiveTransform::antiProject( DTPoint p ){
	DTPoint result;
	
	double den = (c[0]*(double)p.x + c[1]*(double)p.y + 1.0 );
	result.x = (int) ((a[0][0]*(double)p.x + a[0][1]*(double)p.y + a[0][2])/den);
	
	result.y = (int) ((a[1][0]*(double)p.x + a[1][1]*(double)p.y + a[1][2])/den);
	
	return result;
}

void ProjectiveTransform::calculateCalibMatrix( DTPoint calibrationPoint[4], int width, int height ){
	//sort calibration points first
	for(int n=0; n<4;n++){
		if( (calibrationPoint[n].x>width/2)&&(calibrationPoint[n].y<height/2) ){
			DTPoint swp = calibrationPoint[0];
			calibrationPoint[0] = calibrationPoint[n];
			calibrationPoint[n] = swp;
		}
	}
	for(int n=0; n<4;n++){
		if( (calibrationPoint[n].x<width/2)&&(calibrationPoint[n].y<height/2) ){
			DTPoint swp = calibrationPoint[1];
			calibrationPoint[1] = calibrationPoint[n];
			calibrationPoint[n] = swp;
		}
	}
	for(int n=0; n<4;n++){
		if( (calibrationPoint[n].x<width/2)&&(calibrationPoint[n].y>height/2) ){
			DTPoint swp = calibrationPoint[2];
			calibrationPoint[2] = calibrationPoint[n];
			calibrationPoint[n] = swp;
		}
	}
	//calibration points sorted
	
	// recalculate calibration coefficients here
	// should be something like the following:
	
	long H=outHeight;
	long W=outWidth;
	
	double x[4], y[4];
	for(int m=0;m<4;m++){
		x[m] = (double)calibrationPoint[m].x;
		y[m] = (double)calibrationPoint[m].y;
	}
	
	a[0][2] = x[0];
	
	a[1][2] = y[0];
	
	c[0] = (-x[0]*y[3]+x[2]*y[1]-x[3]*y[1]+x[3]*y[0]-x[1]*y[2]+x[0]*y[2]-x[2]*y[0]+x[1]*y[3])
		/W/(-x[3]*y[2]+x[3]*y[1]-x[2]*y[1]+x[1]*y[2]+x[2]*y[3]-x[1]*y[3]);
	
	c[1] = (x[3]*y[2]-x[0]*y[2]-x[2]*y[3]+x[2]*y[0]-x[3]*y[1]+x[0]*y[1]+x[1]*y[3]-x[1]*y[0])
		/(-x[3]*y[2]+x[3]*y[1]-x[2]*y[1]+x[1]*y[2]+x[2]*y[3]-x[1]*y[3])/H;
	
	a[1][1] = (-y[3]*x[2]*y[1]+y[2]*x[1]*y[3]-x[0]*y[2]*y[3]+x[0]*y[3]*y[1]
		+x[3]*y[0]*y[2]-x[3]*y[0]*y[1]+x[2]*y[0]*y[1]-y[2]*x[1]*y[0])
		/H/(-x[3]*y[2]+x[3]*y[1]-x[2]*y[1]+x[1]*y[2]+x[2]*y[3]-x[1]*y[3]);
	
	a[0][1] = -(x[3]*x[2]*y[1]-x[3]*x[1]*y[2]-x[3]*x[2]*y[0]+x[3]*x[1]*y[0]
		-x[0]*x[2]*y[1]+x[0]*x[1]*y[2]+x[0]*x[2]*y[3]-x[1]*x[0]*y[3])
		/H/(-x[3]*y[2]+x[3]*y[1]-x[2]*y[1]+x[1]*y[2]+x[2]*y[3]-x[1]*y[3]);
	
	a[1][0] = (-x[0]*y[3]*y[1]+x[0]*y[1]*y[2]-y[1]*x[3]*y[2]+y[3]*x[2]*y[1]
		+x[3]*y[0]*y[2]-y[2]*x[1]*y[0]-y[3]*x[2]*y[0]+x[1]*y[0]*y[3])
		/W/(-x[3]*y[2]+x[3]*y[1]-x[2]*y[1]+x[1]*y[2]+x[2]*y[3]-x[1]*y[3]);
	
	a[0][0] = (x[3]*x[1]*y[0]-x[1]*x[2]*y[0]-x[3]*x[1]*y[2]+x[1]*x[2]*y[3]
		+x[3]*x[0]*y[2]-x[3]*x[0]*y[1]+x[0]*x[2]*y[1]-x[0]*x[2]*y[3])
		/W/(-x[3]*y[2]+x[3]*y[1]-x[2]*y[1]+x[1]*y[2]+x[2]*y[3]-x[1]*y[3]);
}
