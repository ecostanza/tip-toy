/***************************************************************************
     projectivetransform.h  - 
                             -------------------
    copyright            : (C) 2007 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of DTServer.                                          *
 *                                                                         *
 * DTServer is free software: you can redistribute it and/or modify        *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * DTServer is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with DTServer. If not, see <http://www.gnu.org/licenses/>.        *
 ***************************************************************************/

#ifndef _EC_PROJECTIVETRANSFORM
#define _EC_PROJECTIVETRANSFORM

#include <libdtouch/types.h>

void camera2GL( DTPoint in, int w, int h, float& outx, float& outy  );
void camera2GL( int inx, int iny, int w, int h, float& outx, float& outy  );


class ProjectiveTransform{
protected:
	// calibration coefficients
	double a[2][3];
	double c[2]; 
	int outWidth;
	int outHeight;
public:
	ProjectiveTransform( int in_outWidth=600, int in_outHeight=400 );
	DTPoint project( DTPoint p );
	DTPoint projectFlip( DTPoint p );
    DTPoint projectRotateCW( DTPoint p );
    DTPoint projectRotateCCW( DTPoint p );
    void calculateCalibMatrix( DTPoint calibrationPoint[4], int in_width, int in_height );

	DTPoint antiProject( DTPoint p );

};

#endif
