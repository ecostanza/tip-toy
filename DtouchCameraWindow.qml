/***************************************************************************
    DtouchCameraWindow.qml  -  QML camera and viewfinder for dtouch
                             -------------------
    copyright            : (C) 2019 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of libdtouch.                                         *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

import QtQuick 2.0
import QtMultimedia 5.4
import filter.dtouch.org 1.0
import QtQuick.Window 2.2
import QtWebChannel 1.0
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.5


Window {
    id : cameraWindow

    width: 800
    height: 480
    visible: true

    property DtouchFilter filter: dtouch

    Camera {
        id: camera
        captureMode: Camera.CaptureStillImage

        onCameraStateChanged: {
            console.log('onCameraStateChanged', cameraState)
            //if (cameraState !== Camera.UnloadedState) {
            if (cameraState === Camera.ActiveState) {
                var resolutions = camera.supportedViewfinderResolutions();//;
                var resolution_strings = resolutions.map(function(s) { return s.width + 'x' + s.height;});
                resolutionCombo.model = resolution_strings
                console.log('resolutions', resolution_strings);
                console.log('camera.viewfinder.resolution:', camera.viewfinder.resolution)

            }
            console.log('onCameraStateChanged returning')
        }

    }

    DtouchFilter {
        id: dtouch
        WebChannel.id: "dtouch"

        trackingDistance: trackingDistanceSpinBox.value
        trackingThreshold: trackingThresholdSpinBox.value

        onFinished: {
            trackingDistanceSpinBox.value = trackingDistance
            trackingThresholdSpinBox.value = trackingThreshold
        }

        Component.onCompleted: {
            dtouch.requestPixelFormat(camera)
        }

    }

    RowLayout {
        anchors.fill: parent
        spacing: 6

        VideoOutput {
            id: viewfinder
            visible: true

            filters: [dtouch]

            Layout.preferredWidth: 640
            Layout.preferredHeight: 480

            source: camera
            autoOrientation: true
        }

        // controls
        ColumnLayout {
            //anchors.fill: parent
            Layout.fillHeight: true
            spacing: 6

            ComboBox {
                id: cameraSelectionCombo

                textRole: 'displayName'
                valueRole: 'deviceId'

                model: QtMultimedia.availableCameras
                onActivated: camera.deviceId = currentValue

                Component.onCompleted: currentIndex = indexOfValue(camera.deviceId)
            }

            ComboBox {
                id: resolutionCombo

                model: []

                onActivated: {
                    console.log('resolutionCombo onCurrentValueChanged:', currentValue)
                    if (currentValue) {
                        camera.viewfinder.resolution = currentValue

                        camera.stop()
                        camera.start()
                    }
                }

            }


            Button {
                text: 'Flip'
                checkable: true
                checked: dtouch.flipImage

                onClicked: {
                    if (checked) {
                        dtouch.flipImage = true
                    } else {
                        dtouch.flipImage = false
                    }
                }
            }

            Button {
                text: 'Rotate'
                checkable: true
                checked: dtouch.rotateImage

                onClicked: {
                    if (checked) {
                        dtouch.rotateImage = true
                    } else {
                        dtouch.rotateImage = false
                    }
                }
            }

            Button {
                text: 'Enable blur'
                checkable: true
                checked: dtouch.blur

                onClicked: {
                    if (checked) {
                        dtouch.blur = true
                    } else {
                        dtouch.blur = false
                    }
                }
            }

            Button {
                text: 'Enable Tracking'
                checkable: true
                checked: dtouch.tracking

                onClicked: {
                    if (checked) {
                        dtouch.tracking = true
                        //dtouch.trackingDistance = trackingDistanceSpinBox.value
                        //dtouch.trackingThreshold = trackingThresholdSpinBox.value
                    } else {
                        dtouch.tracking = false
                    }
                }
            }
            Label {
                text: 'distance:'
            }
            SpinBox {
                id: trackingDistanceSpinBox
                //value: dtouch.trackingDistance
            }
            Label {
                text: 'frames:'
            }
            SpinBox {
                id: trackingThresholdSpinBox
                //value: dtouch.trackingThreshold
            }

            Button {
                text: 'Show B/W'
                checkable: true
                checked: dtouch.displayThresholded

                onClicked: {
                    if (checked) {
                        dtouch.displayThresholded = true
                    } else {
                        dtouch.displayThresholded = false
                    }
                }
            }

        }

    }

}
