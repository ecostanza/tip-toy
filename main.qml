/***************************************************************************
    main.qml  -  combine camera and browser
                             -------------------
    copyright            : (C) 2019 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of TIP-Toy.                                           *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

import QtQuick 2.12
import QtQuick.Window 2.12

QtObject {
    id: tipToyRoot

    property DtouchCameraWindow tipToyCameraWindow: DtouchCameraWindow {

    }



    property Browser browserWindow: Browser {
        visible: true
        filter: tipToyCameraWindow.filter
        title: qsTr("TIP-Toy Browser")
    }

}
