/***************************************************************************
     fiducialtracking.h  -  class for tracking fiducial markers 
	                        across frames
                             -------------------
    copyright            : (C) 2009 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of libdtouch.                                         *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

#ifndef EC_FIDUCIALTRACKING
#define EC_FIDUCIALTRACKING

#include "pool.h"
#include "listhash.h"
#include "types.h"


class FiducialTracking
{
public:

	void increaseTrackingThreshold(){_trackingThreshold++;}
	void decreaseTrackingThreshold(){if(_trackingThreshold > 1 )_trackingThreshold--;}

	void increaseDistanceThreshold(){_distanceThreshold++;}
	void decreaseDistanceThreshold(){if(_distanceThreshold > 1 )_distanceThreshold--;}

    int &trackingThreshold(){ return _trackingThreshold; }
    //void setTrackingThreshold(int value){ _trackingThreshold = value; }

    int &distanceThreshold(){ return _distanceThreshold; }
    //void setDistanceThreshold(int value){ _distanceThreshold = value; }

	static FiducialTracking * newL(const int &distance,const int &tracking);
	FiducialTracking(const int &distance,const int &tracking);
	~FiducialTracking(void);

	void initL();

public:
	void process(FiducialDataList * curr);
protected:
	int _distanceThreshold;
	int _trackingThreshold;

	FiducialDataList* _pre;
	FiducialDataList * _filtered;
	int _idCount;
	int generateID();
};

#endif
