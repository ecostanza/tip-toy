/***************************************************************************
    Browser.qml  -  QML based web browser, exposes dtouch results to js
                             -------------------
    copyright            : (C) 2019 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of TIP-Toy.                                           *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtWebEngine 1.7
import QtWebChannel 1.0
import filter.dtouch.org 1.0

Window {
    id: browserRoot
    width: 800; height: 600

    property DtouchFilter filter

    Text {
        anchors.centerIn: parent
        text: qsTr("Hello World.")
    }

    WebChannel {
        id: web_channel

        registeredObjects: [filter]
    }

    WebEngineView {
        id: webEngine
        anchors.fill: parent
        url: "qrc:/html/index.html"
        webChannel: web_channel

        WebEngineScript {
            id: jscript
            sourceUrl: "qrc:/qtwebchannel/qwebchannel.js"
            name: 'qwebchannel.js'
            worldId: WebEngineScript.MainWorld
            injectionPoint: WebEngineScript.DocumentCreation
        }

        profile.userScripts: [jscript]

        onJavaScriptConsoleMessage: function (level, message, lineNumber, sourceId) {
            console.log(level, message, lineNumber, sourceId);
        }

    }

}
