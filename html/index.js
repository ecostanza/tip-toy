/***************************************************************************
    Browser.qml  -  TIP-Toy programming blocks logic
                             -------------------
    copyright            : (C) 2019 by Enrico Costanza
    email                : e.costanza@ieee.org
 ***************************************************************************/

/***************************************************************************
 * This file is part of TIP-Toy.                                           *
 *                                                                         *
 * libdtouch is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * libdtouch is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with libdtouch. If not, see <http://www.gnu.org/licenses/>.       *
 ***************************************************************************/

var DISTANCE_THRESHOLD = 45;

var dist = function (p, q) {
    var dx = p.x - q.x;
    var dy = p.y - q.y;
    return dx * dx + dy * dy;
};

var check_above = function (self, other) {
    if (self.block_above !== undefined) {
        return other === self.block_above;
    }
    var dy = other.bottom_left().y - self.top_left().y;
    if (
        ((self.top_left().x <= other.bottom_left().x & 
        other.bottom_left().x < self.top_right().x) | 
        (self.top_left().x < other.bottom_right().x & 
        other.bottom_right().x <= self.top_right().x)) &
        dy*dy < DISTANCE_THRESHOLD
    ) {
        self.block_above = other;
        other.block_below = self;
        return true;
    } else {
        return false;
    }
};

var check_below = function (self, other) {
    if (self.block_below !== undefined) {
        return other === self.block_below;
    }
    var dy = other.top_left().y - self.bottom_left().y;
    if (
        ((self.bottom_left().x <= other.top_left().x & 
        other.top_left().x < self.bottom_right().x) | 
        (self.bottom_left().x < other.top_right().x & 
        other.top_right().x <= self.bottom_right().x)) &
        dy*dy < DISTANCE_THRESHOLD
    ) {
        self.block_below = other;
        other.block_above = self;
        return true;
    } else {
        return false;
    }
};

var transverse_below = function (first) {
    var current = first,
        tmp,
        result = {
            code: '',
            code_real: '',
            next: undefined
        };
        while(current !== undefined) {
            tmp = current.transverse();
            //console.log('current.name:', current.name);
            if (current.name === 'end_repeat') {
                result.code += "}\nend repeat\n";
                result.next = current.block_below;
                break;
            }
            if (tmp !== undefined) {
                result.code += tmp.code;
                result.code_real += tmp.code_real;
                current = tmp.next
            }
        }
    return result;
};

// BLOCKS!

var basic_block = {
    master: true,

    // TODO: take rotation into account
    bottom_left: function () { return {x: this.x, y: this.y + this.height}; },
    bottom_right: function () { return {x: this.x + this.width, y: this.y + this.height}; },
    top_left: function () { return {x: this.x, y: this.y}; },
    top_right: function () { return {x: this.x + this.width, y: this.y}; },

    // TODO: take rotation into account
    centre: function () { 
        return {
            x: this.x + this.width / 2, 
            y: this.y + this.height / 2
        };
    },

};


var start_block = $.extend({}, basic_block, {
    name: 'start',
    master: 'false',

    width: 135,
    height: 44,

    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #ddffdd; stroke:black"></rect>
        <text 
            x="15" 
            y="26" 
            class="small">START</text>
        `;
    },

    transverse: function () {
        var children_content = transverse_below(this.block_below);

        return {
            'code': children_content.code,
            'code_real': children_content.code_real,
            'next': undefined
        };
    },

    block_below: undefined,
    edges: function () {
        var result = [];
        if (this.block_below !== undefined) {
            result.push({'below': this.block_below});
        }
        return result;
    },

    reset_edges: function () {
        this.block_below = undefined;
    },

    neighbour_checkers: [
        // check below
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            return check_below(this, other);
        },
    ],

});

var variable_block = $.extend({}, basic_block, {
    name: 'variable',
    width: 60,
    height: 30,
    
    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #eeeeee; stroke:black; z-index: 12"></rect>
        <text 
            x="5" 
            y="15" 
            class="small"
            style="z-index: 11">${this.id}</text>
        `;
    },

    block_container: undefined,
    edges: function () {
        var result = [];
        if (this.block_container !== undefined) {
            result.push({'container': this.block_container});
        }
        return result;
    },
    reset_edges: function () {
        this.block_container = undefined;
    },
    transverse: function () {
        // empty?
        throw 'transverse called on variable';
        return undefined;
    },
    neighbour_checkers: [
        // empty?
    ],
});

var container_block = $.extend({}, basic_block, {
    width: 135,
    height: 44,

    container_offset: {y:20 , x: 100},
    container_width: 60,
    container_height: 30,

    // TODO: take rotation into account
    container_centre: function () { 
        return {
            x: this.x + this.container_offset.x, 
            y: this.y + this.container_offset.y
        };
    },

    block_above: undefined,
    block_below: undefined,
    block_contained: undefined,
    edges: function () {
        var result = [];
        if (this.block_above !== undefined) {
            result.push({'above': this.block_above});
        }
        if (this.block_below !== undefined) {
            result.push({'below': this.block_below});
        }
        if (this.block_contained !== undefined) {
            result.push({'contained': this.block_contained});
        }
        return result;
    },

    reset_edges: function () {
        this.block_above = undefined;
        this.block_below = undefined;
        this.block_contained = undefined;
    },

    neighbour_checkers: [
        // check above
        function (other) { 
            if (other.name === 'variable') {
                return false;
            }
            return check_above(this, other);
        },
        // check below
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            return check_below(this, other);
        },
        // check contained
        function (other) { 
            if (other.name !== 'variable') {
                return false;
            }
            if (this.block_contained !== undefined) {
                return other === this.block_contained;
            }
            if (
                dist (this.container_centre(), other.centre()) < DISTANCE_THRESHOLD
                & 
                dist (this.container_centre(), other.centre()) < DISTANCE_THRESHOLD
            ) {
                this.block_contained = other;
                // setting it to true to avoid a loop
                other.block_container = true;
                return true;
            } else {
                return false;
            }
        }

    ],

});

var play_block = $.extend({}, container_block, {
    name: 'play',

    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #ccddff; stroke:black; z-index: 1"></rect>
        <text 
            x="15" 
            y="26" 
            class="small"
            style="z-index: 2">PLAY</text>
        <rect 
            x="${this.container_offset.x - this.container_width/2}"
            y="${this.container_offset.y - this.container_height/2}"
            width="${this.container_width}"
            height="${this.container_height}"
            style="fill: white; stroke:black; z-index: 2"></rect>
        `;
    },

    transverse: function () {
        var code = '',
            code_real = '',
            contained = '';
        if (this.block_contained !== undefined) {
            contained = this.block_contained.id;
            // console.log('this.block_contained.id:', this.block_contained.id);
        }
        code += `play variable ${contained}\n`;
        code_real += `await play(${contained});\n`;
        // console.log('code:', code);
        return {
            'code': code,
            'code_real': code_real,
            'next': this.block_below
        };
    },
});

var repeat_block = $.extend({}, container_block, {
    name: 'repeat',

    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #ffffdd; stroke:black; z-index: 1"></rect>
        <text 
            x="15" 
            y="26" 
            class="small"
            style="z-index: 2">REPEAT</text>
        <rect 
            x="${this.container_offset.x - this.container_width/2}"
            y="${this.container_offset.y - this.container_height/2}"
            width="${this.container_width}"
            height="${this.container_height}"
            style="fill: white; stroke:black; z-index: 2"></rect>
        `;
    },

    transverse: function () {
        var code = '', 
            code_real = '',
            contained = '1';
        if (this.block_contained !== undefined) {
            contained = this.block_contained.id;
        }

        var children_content = transverse_below(this.block_below, true);

        if (children_content.code.includes('end repeat')) {
            code += `repeat(${contained}) times {\n${children_content.code}\n`;
        } else {
            code += `repeat(${contained}) times {\n${children_content.code}\n}\n`;
        }
        
        code_real += `for(var REPEAT=0;REPEAT<${contained};REPEAT+=1) {\n${children_content.code_real}\n}\n`;
        console.log('next:', children_content.next);
        return {
            'code': code,
            'code_real': code_real,
            'next': children_content.next
        };
    },
});

var end_repeat_block = $.extend({}, basic_block, {
    name: 'end_repeat',
    master: true,

    width: 135,
    height: 44,

    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #ffffdd; stroke:black"></rect>
        <text 
            x="15" 
            y="26" 
            class="small">END REPEAT</text>
        `;
    },

    transverse: function () {
        //var children_content = transverse_below(this.block_below);

        return {
            'code': "---\n",
            'code_real': "\n",
            'next': this.block_below
        };
    },

    block_below: undefined,
    edges: function () {
        var result = [];
        if (this.block_below !== undefined) {
            result.push({'below': this.block_below});
        }
        return result;
    },

    reset_edges: function () {
        this.block_below = undefined;
    },

    neighbour_checkers: [
        // check above
        function (other) { 
            if (other.name === 'variable') {
                return false;
            }
            return check_above(this, other);
        },
        // check below
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            return check_below(this, other);
        },
    ],

});

var branching_block = $.extend({}, basic_block, {
    name: 'branching',
    // value: 'play',
    width: 60,
    height: 100,

    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #ddddff; stroke:black; z-index:1"></rect>
        <text 
            x="15" 
            y="26" 
            class="small"
            style="z-index: 2">BRAN</text>
        <text 
            x="15" 
            y="46" 
            class="small"
            style="z-index: 2">CHING</text>
        `;
    },

    block_above: undefined,
    block_below: undefined,
    // block_left: undefined,
    block_right: undefined,
    edges: function () {
        var result = [];
        if (this.block_above !== undefined) {
            result.push({'above': this.block_above});
        }
        if (this.block_below !== undefined) {
            result.push({'below': this.block_below});
        }
        if (this.block_right !== undefined) {
            result.push({'right': this.block_right});
        }
        return result;
    },
    reset_edges: function () {
        this.block_above = undefined;
        this.block_below = undefined;
        this.block_right = undefined;
    },
    transverse: function () {
        var code = '', 
            code_real = '';
        // this is similar to the repeat block
        var condition = 'false';
        var inner_block = '';
        var inner_block_real = '';
        if (this.block_right !== undefined) {
            // condition = this.block_right.name;
            // condition = 'i > 2';
            condition = this.block_right.condition();
            tmp = this.block_right.transverse();
            inner_block += tmp.code;
            inner_block_real += tmp.code_real;
        }
        code += `if (${condition}) {\n${inner_block}\n}\n`;
        code_real += `if (${condition}) {\n${inner_block_real}\n}\n`;

        return {
            'code': code,
            'code_real': code_real,
            'next': this.block_below
        };
    },
    neighbour_checkers: [
        // check above
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            return check_above(this, other);
        },
        // check below
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            return check_below(this, other);
        },
        // check right
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            if (this.block_contained !== undefined) {
                return other === this.block_right;
            }
            var dx = other.bottom_left().x - this.bottom_right().x;
            if (
                // the block needs to be inside this one on the vertical axis
                other.bottom_left().y < this.bottom_right().y &
                other.top_left().y > this.top_right().y &
                // and have its left edge close to the right edge of this on the horizontal
                (dx*dx) < DISTANCE_THRESHOLD
            ) {
                this.block_right = other;
                // setting it to true to avoid a loop
                other.block_left = true;
                return true;
            } else {
                return false;
            }
        }

    ],
});

// TODO: change condition to be a container block
// and the condition to be of the form 'i > variable'
// consider adding also a 'i < variable' block
// add 'else' block which stops tranversal on condition
var condition_block = $.extend({}, container_block, {
    name: 'condition',
    condition: function () {
        var contained = '0';
        if (this.block_contained !== undefined) {
            contained = this.block_contained.id;
        }
        return 'REPEAT > ' + contained;
    },
    width: 135,
    height: 44,

    render: function () { 
        return `
        <rect 
            x="0" 
            y="0" 
            width="${this.width}" 
            height="${this.height}" 
            style="fill: #ffdddd; stroke:black; z-index: 1"></rect>
        <text 
            x="15" 
            y="26" 
            class="small"
            style="z-index: 2">${this.condition()}</text>
        <rect 
            x="${this.container_offset.x - this.container_width/2}"
            y="${this.container_offset.y - this.container_height/2}"
            width="${this.container_width}"
            height="${this.container_height}"
            style="fill: white; stroke:black; z-index: 2"></rect>
        `;
    },

    block_left: undefined,
    block_below: undefined,
    block_contained: undefined,
    edges: function () {
        var result = [];
        if (this.block_left !== undefined) {
            result.push({'left': this.block_left});
        }
        if (this.block_below !== undefined) {
            result.push({'below': this.block_below});
        }
        if (this.block_contained !== undefined) {
            result.push({'contained': this.block_contained});
        }
        return result;
    },
    reset_edges: function () {
        this.block_left = undefined;
        this.block_below = undefined;
        this.block_contained = undefined;
    },
    transverse: function () {
        // this is similar to the repeat block
        var children_content = transverse_below(this.block_below);

        return {
            'code': children_content.code,
            'code_real': children_content.code_real,
            'next': undefined
        };
    },
    neighbour_checkers: [
        // check left
        function (other) { 
            if (other.name !== 'branching') {
                return false;
            }
            if (this.block_left !== undefined) {
                return other === this.block_left;
            }
            var dx = other.bottom_right().x - this.bottom_left().x;
            if (
                // the block needs to be inside the other one on the vertical axis
                this.bottom_left().y < other.bottom_right().y &
                this.top_left().y > other.top_right().y &
                // and have its left edge close to the right edge of this on the horizontal
                (dx*dx) < DISTANCE_THRESHOLD
            ) {
                this.block_left = true;
                // setting it to true to avoid a loop
                other.block_right = this;
                return true;
            } else {
                return false;
            }
        },
        // check below
        function (other) { 
            if (other.name === 'condition' | other.name === 'variable') {
                return false;
            }
            return check_below(this, other);
        },
        // check contained
        function (other) { 
            if (other.name !== 'variable') {
                return false;
            }
            if (this.block_contained !== undefined) {
                return other === this.block_contained;
            }
            if (
                dist (this.container_centre(), other.centre()) < DISTANCE_THRESHOLD
                & 
                dist (this.container_centre(), other.centre()) < DISTANCE_THRESHOLD
            ) {
                this.block_contained = other;
                // setting it to true to avoid a loop
                other.block_container = true;
                return true;
            } else {
                return false;
            }
        },

    ],
});

var _stop_requested = false;

var update = function (markers) {
    //console.log('markers:', markers);
    var tangible = true;
    if (markers === undefined) {
        tangible = false;
        markers = [
            {x: 230, y:  10, id: 0, angle: 0},
            {x:  10, y: 120, id: 11, angle: 0},
            {x:  10, y: 170, id: 12, angle: 0},
            {x:  10, y: 220, id: 14, angle: 0},
            //{x:  10, y: 270, id: 13, angle: 0},
            {x:  10, y:  10, id: 1, angle: 0},
            {x:  80, y:  10, id: 2, angle: 0},
            {x: 150, y:  10, id: 3, angle: 0},
            {x:  10, y:  45, id: 4, angle: 0},
            {x:  80, y:  45, id: 5, angle: 0},
            {x: 150, y:  45, id: 6, angle: 0},
            {x:  10, y:  80, id: 7, angle: 0},
            {x:  80, y:  80, id: 8, angle: 0},
        ];
        // var markers = [{"x":98,"y":232,"id":11,"angle":0},{"x":100,"y":91,"id":11,"angle":0},{"x":100,"y":50,"id":11,"angle":0},{"x":100,"y":10,"id":12,"angle":0},{"x":118,"y":132,"id":13,"angle":0},{"x":237,"y":168,"id":14,"angle":0},{"x":220,"y":10,"id":1,"angle":0},{"x":167,"y":242.99999237060547,"id":2,"angle":0},{"x":220,"y":70,"id":3,"angle":0},{"x":170,"y":102,"id":4,"angle":0},{"x":250,"y":10,"id":5,"angle":0},{"x":172,"y":62,"id":6,"angle":0},{"x":250,"y":70,"id":7,"angle":0},{"x":250,"y":100,"id":8,"angle":0}];
        // var markers = [{"x":100,"y":91,"id":11,"angle":0},{"x":100,"y":50,"id":11,"angle":0},{"x":316,"y":79,"id":12,"angle":0},{"x":377,"y":145,"id":13,"angle":0},{"x":237,"y":168,"id":14,"angle":0},{"x":220,"y":10,"id":1,"angle":0},{"x":172,"y":141.99999237060547,"id":2,"angle":0},{"x":220,"y":70,"id":3,"angle":0},{"x":170,"y":102,"id":4,"angle":0},{"x":250,"y":10,"id":5,"angle":0},{"x":172,"y":62,"id":6,"angle":0},{"x":250,"y":70,"id":7,"angle":0},{"x":250,"y":100,"id":8,"angle":0},{"x":99,"y":132,"id":11,"angle":0}];
        // var markers = [{"x":208,"y":189,"id":11,"angle":0},{"x":97,"y":191,"id":11,"angle":0},{"x":207,"y":148.99999237060547,"id":11,"angle":0},{"x":100,"y":50,"id":11,"angle":0},{"x":100,"y":10,"id":12,"angle":0},{"x":114,"y":90,"id":13,"angle":0},{"x":175,"y":109,"id":14,"angle":0},{"x":220,"y":10,"id":1,"angle":0},{"x":170,"y":202.99999237060547,"id":2,"angle":0},{"x":280,"y":200.99999237060547,"id":3,"angle":0},{"x":279,"y":162.99999237060547,"id":4,"angle":0},{"x":250,"y":10,"id":5,"angle":0},{"x":172,"y":62,"id":6,"angle":0},{"x":250,"y":70,"id":7,"angle":0},{"x":173,"y":22,"id":8,"angle":0}];
        // var markers = [{"x":99,"y":9,"id":10,"angle":0},{"x":215,"y":189,"id":11,"angle":0},{"x":95,"y":229,"id":11,"angle":0},{"x":207,"y":148.99999237060547,"id":11,"angle":0},{"x":100,"y":50,"id":11,"angle":0},{"x":95,"y":189.99999237060547,"id":12,"angle":0},{"x":114,"y":90,"id":13,"angle":0},{"x":175,"y":109,"id":14,"angle":0},{"x":220,"y":10,"id":1,"angle":0},{"x":165,"y":239,"id":2,"angle":0},{"x":285,"y":199,"id":3,"angle":0},{"x":279,"y":162.99999237060547,"id":4,"angle":0},{"x":165,"y":199.99999237060547,"id":5,"angle":0},{"x":172,"y":62,"id":6,"angle":0},{"x":250,"y":70,"id":7,"angle":0},{"x":398,"y":42,"id":8,"angle":0}];
        // var markers = [{"x":99,"y":9,"id":10,"angle":0},{"x":243,"y":219,"id":11,"angle":0},{"x":95,"y":229,"id":11,"angle":0},{"x":214,"y":178.99999237060547,"id":11,"angle":0},{"x":95,"y":89,"id":11,"angle":0},{"x":96,"y":50,"id":12,"angle":0},{"x":112,"y":128.99999237060547,"id":13,"angle":0},{"x":174,"y":140,"id":14,"angle":0},{"x":243,"y":149.99999237060547,"id":1,"angle":0},{"x":165,"y":239,"id":2,"angle":0},{"x":313,"y":229,"id":3,"angle":0},{"x":284,"y":188.99999237060547,"id":4,"angle":0},{"x":166,"y":60,"id":5,"angle":0},{"x":165,"y":99,"id":6,"angle":0},{"x":251,"y":75,"id":7,"angle":0},{"x":398,"y":42,"id":8,"angle":0}];
        //markers = [{"x":200,"y":10,"id":0,"angle":0},{"x":10,"y":170,"id":13,"angle":0},{"x":40,"y":10,"id":2,"angle":0},{"x":100,"y":10,"id":4,"angle":0},{"x":40,"y":40,"id":6,"angle":0},{"x":70,"y":40,"id":7,"angle":0},{"x":198,"y":54,"id":11,"angle":0},{"x":10,"y":120,"id":12,"angle":0},{"x":198,"y":97,"id":12,"angle":0},{"x":199,"y":142,"id":11,"angle":0},{"x":199,"y":187,"id":14,"angle":0},{"x":10,"y":10,"id":1,"angle":0},{"x":269,"y":148,"id":1,"angle":0},{"x":10,"y":70,"id":11,"angle":0},{"x":198,"y":233,"id":11,"angle":0},{"x":100,"y":40,"id":8,"angle":0},{"x":268,"y":239,"id":8,"angle":0},{"x":70,"y":10,"id":3,"angle":0},{"x":268,"y":102,"id":3,"angle":0},{"x":10,"y":40,"id":5,"angle":0},{"x":269,"y":59,"id":5,"angle":0}];

        //markers = [{"x":230,"y":10,"id":0,"angle":0},{"x":150,"y":10,"id":3,"angle":0},{"x":10,"y":45,"id":4,"angle":0},{"x":80,"y":45,"id":5,"angle":0},{"x":150,"y":45,"id":6,"angle":0},{"x":10,"y":80,"id":7,"angle":0},{"x":80,"y":80,"id":8,"angle":0},{"x":10,"y":170,"id":12,"angle":0},{"x":231,"y":54,"id":12,"angle":0},{"x":298,"y":59,"id":2,"angle":0},{"x":232,"y":98,"id":11,"angle":0},{"x":10,"y":220,"id":14,"angle":0},{"x":232,"y":142,"id":14,"angle":0},{"x":10,"y":120,"id":11,"angle":0},{"x":233,"y":186,"id":11,"angle":0},{"x":10,"y":10,"id":1,"angle":0},{"x":302,"y":103,"id":1,"angle":0},{"x":80,"y":10,"id":2,"angle":0},{"x":302,"y":190,"id":2,"angle":0}];
        //markers = [{"x":230,"y":10,"id":0,"angle":0},{"x":80,"y":45,"id":5,"angle":0},{"x":150,"y":45,"id":6,"angle":0},{"x":10,"y":80,"id":7,"angle":0},{"x":80,"y":80,"id":8,"angle":0},{"x":230,"y":55,"id":11,"angle":0},{"x":10,"y":10,"id":1,"angle":0},{"x":300,"y":61,"id":1,"angle":0},{"x":10,"y":170,"id":12,"angle":0},{"x":233,"y":100,"id":12,"angle":0},{"x":302,"y":106,"id":3,"angle":0},{"x":233,"y":144,"id":11,"angle":0},{"x":80,"y":10,"id":2,"angle":0},{"x":304,"y":150,"id":2,"angle":0},{"x":235,"y":188,"id":11,"angle":0},{"x":150,"y":10,"id":3,"angle":0},{"x":305,"y":194,"id":3,"angle":0},{"x":10,"y":220,"id":14,"angle":0},{"x":236,"y":233,"id":14,"angle":0},{"x":10,"y":120,"id":11,"angle":0},{"x":239,"y":278,"id":11,"angle":0},{"x":10,"y":45,"id":4,"angle":0},{"x":308,"y":283,"id":4,"angle":0}];
        markers = [{"x":230,"y":10,"id":0,"angle":0},{"x":10,"y":45,"id":4,"angle":0},{"x":150,"y":45,"id":6,"angle":0},{"x":80,"y":80,"id":8,"angle":0},{"x":231,"y":55,"id":11,"angle":0},{"x":300,"y":62,"id":1,"angle":0},{"x":10,"y":170,"id":12,"angle":0},{"x":232,"y":100,"id":12,"angle":0},{"x":80,"y":10,"id":2,"angle":0},{"x":302,"y":106,"id":2,"angle":0},{"x":232,"y":145,"id":11,"angle":0},{"x":302,"y":150,"id":3,"angle":0},{"x":10,"y":220,"id":14,"angle":0},{"x":233,"y":189,"id":11,"angle":0},{"x":235,"y":234,"id":14,"angle":0},{"x":303,"y":193,"id":5,"angle":0},{"x":235,"y":279,"id":11,"angle":0},{"x":10,"y":80,"id":7,"angle":0},{"x":306,"y":284,"id":7,"angle":0},{"x":235,"y":324,"id":11,"angle":0},{"x":150,"y":10,"id":3,"angle":0},{"x":305,"y":329,"id":3,"angle":0},{"x":10,"y":120,"id":11,"angle":0},{"x":235,"y":368,"id":11,"angle":0},{"x":80,"y":45,"id":5,"angle":0},{"x":425,"y":331,"id":5,"angle":0},{"x":10,"y":10,"id":1,"angle":0},{"x":305,"y":374,"id":1,"angle":0}];
    } else {
        //console.log('removing blocks')
        d3.selectAll(".block").remove();
    }

    var lut = {
        0: start_block,
        11: play_block,
        12: repeat_block,
        13: branching_block,
        //14: condition_block,
        14: end_repeat_block,
    };
    d3.range(1, 10).forEach(function (i) {lut[i] = variable_block;});

    markers = markers.filter(function (m, i) {
        //console.log('id:', m.id);
        return lut.hasOwnProperty(m.id);
    });
    var blocks = markers.map(function (b, i) {return $.extend({uid: i}, b, lut[b.id]);});
//    if (tangible===true) {
//        blocks.forEach(function (b) {b.master=false;});
//    }

    var _code;
    var _pseudo_code;
    var compute_layout = function () {
        var blocks = d3.selectAll(".block").data();
        //console.log('compute_layout', blocks);
        // reset the edges of the graph
        blocks.forEach(function (b1) {
            b1.reset_edges();
        });
        blocks.forEach(function (b1) {
            blocks.forEach(function (b2) {
                if (b1 === b2) {
                    return;
                }
                b1.neighbour_checkers.forEach(function (check) {
                    // console.log('check:', b1, b2);
                    // console.log('check:', check);
                    var tmp = check.apply(b1, [b2,]);
                    // if (tmp) {
                    //     console.log(b1, b2, 'neighbours');
                    // }
                });
            });
        });

        // navigate graph to extract code!

        // var isolated_blocks = blocks.filter(function (b) {
        //     return b.edges().length === 0;
        // });
        // console.log('isolated_blocks:', isolated_blocks.map(b => {return b.name}));

        // var connected_blocks = blocks.filter(function (b) {
        //     return b.edges().length > 0;
        // });
        // console.log('connected_blocks:', connected_blocks.map(b => {return b.name}));

        // look for the blocks that have nothing on top, they are starters
        var starters = blocks.filter(function (b) {
            return b.name === 'start';
        });
        if (starters.length > 1) {
            throw "more than one start block!";
        }
        // console.log('starters:', starters.map(b => {return b.name}));
        // for each starter follow the chain
        var code = '';
        var code_real = '';
        starters.forEach(function (start) {
            // console.log('forEach:', start.name, index);
            var children_content = start.transverse();

            code += children_content.code;
            code_real += children_content.code_real;
        });
        code = code.replace("\n\n", "\n").replace("\n\n", "\n");
        _code = code_real;
        _pseudo_code = code;
        var markers = blocks.map(function (b) {
            return {x: b.x, y: b.y, id: b.id, angle: b.angle};
        });
        //console.log('markers:', markers);
        var text = '';
        text += code;
        //text += "\n\n-----\n" + code_real;
        // text += "\n\n-----\nmarkers: ";
        // text += JSON.stringify(markers);
        if (d3.select("#code").text() !== text) {
            d3.select("#code").text(text);
        }
        d3.select("#markers").text(JSON.stringify(markers));
    };

    var run_code = function () {
        _stop_requested = false;
        var play_string = `
        function play(sound_id) {
            return new Promise(function(resolve, reject) { // return a promise
                var audio = document.getElementById('sound' + sound_id);                     // create audio wo/ src
                audio.onerror = reject;                      // on error, reject
                audio.onended = resolve;                     // when done, resolve
                if (_stop_requested === true) {
                    reject();
                }
                audio.play();
            });
        }
        `;
        // var re = /play/gi;
        // var code = _code.replace(re, 'await play');
        var f_string = `
        async function play_all () {
            ${_code}
        }
        play_all();
        `;
        // console.log(_code);
        // console.log(code);
        // console.log(f_string);
        Function(play_string + f_string)();
    };

    d3.select('.run-btn').on('click', run_code);
    d3.select('body').on('keydown', function () {
        console.log(d3.event.keyCode);
        if (d3.event.keyCode === 87) {
            run_code();
        } else if (d3.event.keyCode === 83) {
            //run_code();
            //speak('hello world!');
            speak(_pseudo_code, {speed: 150, wordgap: 20});
        } else if (d3.event.keyCode === 32) {
            _stop_requested = true;
            // document.getElementById("player").pause();
            // document.getElementById("player").currentTime = 0;
            let items = d3.selectAll("audio").nodes();
            items.forEach(function (item) {
                item.pause();
                item.currentTime = 0;
            }); 
        }
    });

    var translate = function (d) {
        return `translate(${d.x}, ${d.y})`;
    };

    var svg = d3.select(".chart");
    
    blocks.sort(function (a, b) { return a.id - b.id;});
    //console.log('blocks:', blocks);
    var selection = svg.selectAll(".block")
        .data(blocks, function(d) { return d; });

    // based on: https://stackoverflow.com/questions/36873786/appending-complex-svg-elements-to-a-selection
    selection = selection.enter().append(function (d) {
            // console.log(d.render());
            //create a node
            var g = document.createElementNS("http://www.w3.org/2000/svg", "g");
            
            // fill with the HTML
            g.innerHTML = d.render();

            return g;})
        .attr('id', function (d) {return 'i' + d.uid;})
        .attr('transform', translate)
        .style('cursor', 'grab')
        .attr('class', function (d) {return d.name + ' block';});
        //.classed('block', true);
    svg.selectAll(".variable").raise();

    if (tangible===false) {
        selection.call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));
    }

    function dragstarted(d) {
        console.log('dragstarted');

        // var clone = d3.select(this).node().cloneNode(true);
        // d3.select(this).node().parentElement.append(clone);
        // console.log(d3.select(this).data())
        // console.log(d3.select(this).datum())
        var block = d3.select(this).datum();
        if (block.name === 'start') {
            return;
        }
        if (block.master === true) {
            var block_clone = $.extend({}, block);
            block.master = false;
            block.uid = blocks.length;
            block_clone.master = true;
            d3.select(this).attr('id', 'i' + block.uid);
            blocks.push(block_clone);
            //console.log('datum:', d3.select(this).datum());
            svg.selectAll(".block")
                .data(blocks, function (d) {return d.uid;})
                .enter()
                 .append(function (d) {
                    // console.log(d.render());
                    //create a node
                    var g = document.createElementNS("http://www.w3.org/2000/svg", "g");

                    // fill with the HTML
                    g.innerHTML = d.render();

                    return g;})
                    .attr('id', function (d) {
                        //console.log('enter d.uid:',d.uid);
                        return 'i' + d.uid;})
                    .attr('transform', translate)
                    .style('cursor', 'grab')
                    .classed('block', true)
                    .call(d3.drag()
                        .on("start", dragstarted)
                        .on("drag", dragged)
                        .on("end", dragended));
        }

        d3.select(this)
            // .attr('id', function (d) {return 'i_';})
            .raise()
            .style('cursor', 'grabbing')
            .classed("active", true);
        if (d.block_contained !== undefined) {
            d3.select('#i'+d.block_contained.uid)
                .raise()
                .classed("active", true);
        }
    }

    function dragged(d) {
        // console.log('dragged');
        var block = d3.select(this).datum();
        if (block.name === 'start') {
            return;
        }

        d.x = d3.event.x;
        d.y = d3.event.y;
        d3.select(this).attr('transform', translate);
        if (d.block_contained !== undefined) {
            d.block_contained.x = d3.event.x + d.container_offset.x - d.container_width/2;
            d.block_contained.y = d3.event.y + d.container_offset.y - d.container_height/2;
            d3.select('#i'+d.block_contained.uid)
                .attr('transform', translate);
        }
    }

    function dragended(d, i, n) {
        var block = d3.select(this).datum();
        if (block.name === 'start') {
            return;
        }
        compute_layout();
        d3.select(this)
            .style('cursor', 'grab')
            .classed("active", false);
        if (d.block_contained !== undefined) {
            d3.select('#i'+d.block_contained.uid)
                .style('cursor', 'grab')
                .classed("active", false);
        }
    }

    selection.exit().remove();

    compute_layout();

};

var type_lut = {
    1234: 0,
    1243: 1,
    1324: 2,
    1342: 3,
    1423: 4,
    1432: 5,

    2134: 6,
    2143: 7,
    2314: 8,
    2341: 9,
    2413: 10,
    2431: 11,

    3214: 12,
    3241: 13,
    3124: 14,
    3142: 15,
    3421: 16,
    3412: 17,

    4231: 18,
    4213: 19,
    4321: 20,
    4312: 21,
    4123: 22,
    4132: 23
};

var onData = function (data) {
    //console.log('onData', JSON.stringify(arguments));
    var markers = [];

    for (let i=0; i<data.markers.length; i++) {
        if (data.markers[i].type === 33) {
            continue;
        }

        data.markers[i].id = type_lut[data.markers[i].type];
//        data.markers[i].x = data.markers[i].x * d3.select('svg').style('width').slice(0, -2);
//        data.markers[i].y = data.markers[i].y * d3.select('svg').style('height').slice(0, -2);
//        data.markers[i].x = data.markers[i].x;
//        data.markers[i].y = data.markers[i].y;
        data.markers[i].x = data.markers[i].x * 270;
        data.markers[i].y = data.markers[i].y * 420;
        markers.push(data.markers[i]);
//        console.log('x: ' + data.markers[i].x +
//                    ' y:' + data.markers[i].y +
//                    ' angle:' + data.markers[i].angle +
//                    ' type:' + data.markers[i].type +
//                    ' origType:' + data.markers[i].origType +
//                    ' width:' + data.markers[i].width +
//                    ' height:' + data.markers[i].height);
    }

//    d3.select("#markers").text("onData:" + JSON.stringify(arguments));
    update(markers);
};

var load_sounds = function (folder_name) {
    var i;
    for (i=1; i<9; i+=1) {
        let fname = `sounds/${folder_name}/sound0${i}.mp3`;
        d3.select("#sound" + i).attr('src',fname);
    }
    d3.select("#audioSet").text(folder_name);
    
};

d3.select(window).on("load", function () {
    d3.select("#code").text("nothing yet..");

    var container_width = d3.select('body').style('width').slice(0, -2);
    // var container_height = d3.select('body').style('height').slice(0, -2);

    var svg = d3.select(".chart")
        .attr("width", 270)
        .attr("height", 520);

    load_sounds('PianoNotes');
    d3.select('.piano-btn').on('click', function () {load_sounds('PianoNotes');});
    d3.select('.twinkle-btn').on('click', function () {load_sounds('TwinkleTwinkle');});
    d3.select('.row-btn').on('click', function () {load_sounds('RowYourBoat');});

    //update();
    try {
        if (qt !== undefined) {
            new QWebChannel(qt.webChannelTransport, function (channel) {
                console.log('QWebChannel constructor')
                console.log('channel.objects', channel.objects)
                var dtouch = channel.objects.dtouch;
                d3.select("#markers").text("dtouch.blur:" + JSON.stringify(channel.objects));
                var connection_result = channel.objects.dtouch.finished.connect(onData);
                console.debug('connection_result:', JSON.stringify(connection_result));
            });
        }
    } catch (error) {
        //console.log('error:', error);
        var svg = d3.select(".chart")
            .attr("width", 570)
            .attr("height", 520);
        update();   
    }

});


