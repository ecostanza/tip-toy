QT += quick qml multimedia
QT += webengine webchannel

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    libdtouch/fiducialdata.cpp \
    libdtouch/fiducialrecognition.cpp \
    libdtouch/fiducialTracking.cpp \
    libdtouch/graphicimage.cpp \
    libdtouch/intlist.cpp \
    libdtouch/point.cpp \
    libdtouch/ragbuilder.cpp \
    libdtouch/region.cpp \
    libdtouch/regionadjacencygraph.cpp \
    libdtouch/thresholdfilter.cpp \
    dtouchwrapper.cpp \
    utils/convolver.cpp \
    utils/projectivetransform.cpp \
    main.cpp

HEADERS += \
    libdtouch/fiducialdata.h \
    libdtouch/fiducialrecognition.h \
    libdtouch/fiducialTracking.h \
    libdtouch/graphicimage.h \
    libdtouch/intlist.h \
    libdtouch/list.h \
    libdtouch/listhash.h \
    libdtouch/listpool.h \
    libdtouch/point.h \
    libdtouch/pool.h \
    libdtouch/ragbuilder.h \
    libdtouch/region.h \
    libdtouch/regionadjacencygraph.h \
    libdtouch/stack.h \
    libdtouch/thresholdfilter.h \
    libdtouch/types.h \
    dtouchwrapper.h \
    utils/convolver.h \
    utils/projectivetransform.h

RESOURCES += \
    qml.qrc \
    programmingtui.qrc

QMAKE_INFO_PLIST = Info.plist

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

